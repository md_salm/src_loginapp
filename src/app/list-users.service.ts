import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListUsersService {

  constructor(private _httpClient: HttpClient) { }

  getUsersList(): Observable<any>{
  	return this._httpClient.get("https://reqres.in/api/users?page=1")
  }

  getUsersById(id:number): Observable<any>{
    return this._httpClient.get("https://reqres.in/api/users/"+id);
  }

  deleteUser(id:number): Observable<void>{
  	console.log("https://reqres.in/api/users/"+id)
    return this._httpClient.delete<void>("https://reqres.in/api/users/"+id);
  }
}
