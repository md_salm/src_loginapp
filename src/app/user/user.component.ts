import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListUsersService } from '../list-users.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  statusMessage: string;
  user: any;

  constructor(private _activatedRoute: ActivatedRoute, private _userService: ListUsersService, private _location: Location, private _router: Router) { }

  ngOnInit() {
  	let id:number = this._activatedRoute.snapshot.params['id'];
  	this._userService.getUsersById(id).subscribe((res)=>{
  		this.user = res.data;
  		console.log(this.user)
  	},error => {
  		this.statusMessage = "Problem With The Service"
  		console.log(error);
  		if (error.status == 404) {
  		   this._router.navigate(['admin']);
  		}
  	})
  }

   backClicked() {
    this._location.back();
  }

}
