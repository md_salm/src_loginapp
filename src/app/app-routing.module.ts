import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
  		{
  			path: 'login',
  			component: LoginComponent
  		},
  		{
  			path: 'admin',
  			component: AdminComponent,
  			canActivate: [AuthGuard]
  		},
  		{
  			path: 'register',
  			component: RegisterComponent
  		},
  		{
  			path: 'admin/:id',
  			component: UserComponent,
  		},
  		{
  			path: '',
  			component: HomeComponent
  		},
  	])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
