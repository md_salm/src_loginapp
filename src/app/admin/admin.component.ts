import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ListUsersService } from '../list-users.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  message = "Loading...";
  data:any;
  filteredData:any;
  private _searchTerm: string;

  constructor(private _auth: AuthService, private _userService: ListUsersService) { }

  get searchTerm():string{
    return this._searchTerm;
  }

  set searchTerm(value: string){
    this._searchTerm = value;
    this.filteredData = this.filtereData(value); 
  }

  filtereData(searchString: string){
     return this.data.filter(element => element.first_name.toLowerCase()
       .indexOf(searchString.toLowerCase()) !== -1 ||  element.last_name.toLowerCase()
       .indexOf(searchString.toLowerCase()) !== -1);
  }

  ngOnInit() {
  	this.getUsers();
  }

  getUsers(){
  	this._userService.getUsersList().subscribe((res) => {
  		this.data = res.data;
  		this.filteredData = this.data;
  		console.log(this.data)
  	})
  }

  getUserById(){
  	this._userService.getUsersList().subscribe((res) => {
  		this.data = res.data;
  		console.log(this.data)
  	})
  }

  deleteUser(id){
  	let confirmDelete = confirm("Are you sure you want to delete user?");
	  if (confirmDelete) {
	  	this._userService.deleteUser(id).subscribe((res) => console.log(res)
	  		,(error) => {
	  		console.log(error)
	  	});
	  }
  }

  logout(){
  	this._auth.logout();
  }
}
