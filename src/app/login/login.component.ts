import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // data: any;
  constructor(private _auth: AuthService, private _router: Router) { }

  ngOnInit() {
  }

  loginUser(event){
  	event.preventDefault()
  	const target = event.target;
  	const email = target.querySelector('#email').value;
  	const password = target.querySelector('#password').value;
  	this._auth.getUserDetails(email, password).subscribe(res => {
  		this._router.navigate(['admin']);
  		this._auth.setLoggedIn(true)
  	},error =>{
  		alert(error.error.error);
  	})
  }
}
