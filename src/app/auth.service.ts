import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInStatus = JSON.parse(localStorage.getItem("loggedIn") || 'false')

  constructor(private _http: HttpClient, private _router: Router) { }

  setLoggedIn(val: boolean){
  	this.loggedInStatus = val;
  	localStorage.setItem('loggedIn', 'true')
  }

  get isloggedIn(){
  	return JSON.parse(localStorage.getItem("loggedIn") || this.loggedInStatus.toString())
  }

  getUserDetails(email, password){
  	return this._http.post('https://reqres.in/api/login', {
  		email,
  		password
  	})
  }

  register(email, password){
  	return this._http.post('https://reqres.in/api/register', {
  		email,
  		password
  	})
  }

  logout(){
  	localStorage.removeItem('loggedIn');
  	this._router.navigate(['']);
  }
}
