import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private _auth: AuthService, private _router: Router) { }

  ngOnInit() {
  }

  signUp(event){
  	event.preventDefault()
  	const target = event.target;
  	const email = target.querySelector('#email').value;
  	const password = target.querySelector('#password').value;
  	this._auth.register(email, password).subscribe(res => {
  		this._router.navigate(['admin']);
  		this._auth.setLoggedIn(true)
  		console.log(res)
  	},error =>{
  		alert(error.error.error);
  	})
  }

}
